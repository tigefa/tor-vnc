Tor Browser accessible with VNC. Based much on excellent jfrazelle's tor-browser build.

How to run:

```bash
docker run -it -p 5901:5901 registry.gitlab.com/tigefa/tor-vnc:latest

docker run -d --name torvnc --restart always -p 5901:5901 registry.gitlab.com/tigefa/tor-vnc:latest
```

then connect to :1 display with vncviewer
