FROM tigefa/bionic

# Install vnc in order to create a 'fake' display and firefox, tor-browser packages (get deps)
# We need sudo because some post install stuff runs with tor
#COPY sources.list /etc/apt/sources.list
RUN apt-get update -yqq && apt-get dist-upgrade -yqq
RUN apt-get update && \
	apt-get install -y sudo apt-utils wget curl apt-transport-https dirmngr locales locales-all xz-utils gnupg gnupg2 tightvncserver xterm fluxbox ca-certificates \
	libasound2 libdbus-glib-1-2 libgtk2.0-0 libgtk2.0-dev libgtk-3-dev libxrender1 libxt6 tzdata netcat aria2 whois figlet git p7zip p7zip-full zip unzip

RUN sudo ln -fs /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
RUN sudo dpkg-reconfigure -f noninteractive tzdata

ENV TOR_VERSION 8.0.3
ENV TOR_FINGERPRINT 0x4E2C6E8793298290

#### Get and extract Tor Browser bundle with verification####
RUN wget -q "https://dist.torproject.org/torbrowser/${TOR_VERSION}/tor-browser-linux64-${TOR_VERSION}_en-US.tar.xz" && \
	tar xf "tor-browser-linux64-${TOR_VERSION}_en-US.tar.xz" && \
	rm -rf "tor-browser-linux64-${TOR_VERSION}_en-US.tar.xz"
####/Bundle####
# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

####user section####
ENV USER developer
ENV HOME "/home/$USER"
RUN echo 'developer ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo '%developer ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo 'sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo '%sudo ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo 'www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers && \
    echo '%www-data ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
RUN useradd --create-home --home-dir $HOME --shell /bin/bash $USER && \
	mkdir $HOME/.vnc/
RUN usermod -aG sudo $USER && \
    usermod -aG root $USER && \
    usermod -aG adm $USER && \
    usermod -aG www-data $USER
COPY vnc.sh $HOME/.vnc/
COPY xstartup $HOME/.vnc/
RUN chmod 760 $HOME/.vnc/vnc.sh $HOME/.vnc/xstartup && \
	chown -fR $USER:$USER $HOME tor-browser_en-US

USER "$USER"
###/user section####
RUN mkdir -p /home/$USER/.local/bin
RUN cd /home/$USER/.local/bin && wget https://bin.equinox.io/c/4VmDzA7iaHb/ngrok-stable-linux-amd64.zip; unzip *.zip; rm -rf *.zip
RUN cd $HOME && mkdir -p $HOME/.config/powerline-shell/ && wget https://gitlab.com/snippets/1750446/raw -O $HOME/.config/powerline-shell/config.json
#RUN cd /home/$USER/.local/bin && wget https://download.docker.com/linux/static/stable/x86_64/docker-18.06.1-ce.tgz && tar --strip 1 -xf docker-18.06.1-ce.tgz && rm -rf docker-18.06.1-ce.tgz
#RUN cd /home/$USER/.local/bin && curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl && chmod +x ./kubectl
RUN sudo chown -fR $USER:$USER $HOME

####Setup a VNC password####
RUN	echo vncpassw | vncpasswd -f > ~/.vnc/passwd && \
	chmod 600 ~/.vnc/passwd

EXPOSE 5901
####/Setup VNC####

CMD ["/home/developer/.vnc/vnc.sh"]
